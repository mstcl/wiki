---
title: 时间花
showHeader: true
layout: list
---

Here lies the junction between cyberspace and meatspace, containing various
notes, logs, and ideas while the labourers finally get their rest at the end of
the day to indulge in their moments of unproductivity.

Built by embracing dialectics,
[suboptimality](https://theconvivialsociety.substack.com/p/the-limits-of-optimization),
inefficiency and disorganization.

> Capital is dead labour, that, vampire-like, only lives by sucking living
> labour, and lives the more, the more labour it sucks. The time during which
> the labourer works, is the time during which the capitalist consumes the
> labour-power he has purchased of him.
>
> If the labourer consumes his disposable time for himself, he robs the
> capitalist.

<pre style="background-color: transparent; border-width: 0px; line-height: .9rem"><code style="background-color: transparent">
             v  ~.      v
    v           /|
               / |          v
        v     /__|__
            \--------/
~~~~~~~~~~~~~`~~~~~~'~~~~~~~~~~~~~~~
</code></pre>
