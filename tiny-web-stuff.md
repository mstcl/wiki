---
title: tiny web stuff
description: homemade little thingybobs
---

[pher](https://github.com/mstcl/pher) - tiny wiki ssg

[aquamarine](https://github.com/mstcl/aquamarine) - tiny subsonic client

[cider](https://github.com/mstcl/cider) - tiny ipv4 visualization

[wolly](https://github.com/mstcl/wolly) - tiny wake-on-lan relay

## additional shoutout to

[legit](https://github.com/icyphox/legit) - (tiny) web frontend for git

[dir2opds](https://github.com/dubyte/dir2opds) - (tiny) opds server
