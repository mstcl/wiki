---
title: tuna pasta bake
description: "dense & cheesy"
date: 2023-04-04
toc: true
tags:
  - western-food
---

## ingredients

* 1 jar pasta sauce for tuna bake (or mix part passata part cream cheese with some herbs)
* 300 g pasta of any kind
* 2 tins canned tuna
* 4 cloves garlic leave the peels on
* dried herbs
* garlic powder if need extra kick
* chilli flakes for spice
* salt & pepper to taste
* olive oil drizzle everywhere, idk
* 50 g cheddar chunks or shredded, whichever is easier
* 100 g mushroom closed cup or button
* 1 whole chilli sliced
* 1 whole brown onion sliced

## instructions

1. Lay dried pasta onto tray, add all ingredients except the sauce and seasoning.
1. Don’t forget to add some brine from tuna.
1. Add sauce and dried seasoning, layer on top of pasta.
 Drizzle some olive oil generously.
1. Fill tray with water until pasta is covered.
1. Bake in oven (180 deg C) for about 45 minutes
