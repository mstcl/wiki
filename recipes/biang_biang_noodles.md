---
title: biang biang noodles
description: also known as hot oil noodles
date: 2024-04-04
toc: true
tags:
  - asian-food
---

This is a simple method to get some really fragrant noodles. Instead of
stir-frying the aromatic ingredients, we pour hot oil over them and the
noodles. This results in a bright, fragrant bowl of noodles that can be served
with anything else.

______________________________________________________________________

## ingredients

- Ginger, chopped
- Spring onion, chopped
- Garlic, chopped
- Sichuan peppercorns, crushed
- Vegetable oil

## instructions

1. Make noodles
1. Heat up vegetable oil in a pan
1. Add all of the ingredients to the top of your noodles in a small pile
1. Pour the hot oil over the aromatic ingredients, try to cover all of them.
1. Mix the noodles

## notes

- Combo with the [[sweet_bean_paste_noodles|sweet bean paste noodles]] sauce
  for even more flavourful noodles.
- Serve with some stir-fried tofu on the side for a main meal.
