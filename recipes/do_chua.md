---
title: đồ chua
description: sour stuff
date: 2024-05-05
toc: true
tags:
  - asian-food
---

Vietnamese pickled vegetables, for addition to Banh Mi and other dishes.
Measurements are for a small Kilner-style "clip top" jar.

----------------

## ingredients

- Daikon radish, peeled, roughly 15cm long, 5cm diameter.
- Carrots, peeled, medium size, x2.
- Salt, teaspoon.
- Sugar, 100g + 1 teaspoon.
- Rice vinegar, 220 mL.
- Boiled water, 60 mL.

## aseptic technique

- Put the jar into a pan with some water (does not need to be full), add a lid, and boil for ~10 minutes, whilst you are cutting the vegetables.
- Don't lean over or pass your hands/arms over the open jar. Keep the lid closed whenever you can.
- Clean your hands before handling the vegetables.
- You can sterilise any (metal) implements by using a flame from the hob, if you want to be extra cautious.
- When taking the pickles out of the jar, try not to introduce any foreign items back into the jar.

## instructions

1. Finely chop the Daikon radish and the carrots into thin sticks.
    a. This can be done with just a knife, or with special tools (Julienne slicer, Mandoline).
2. Add the chopped vegetables to a mixing bowl and add the teaspoon of salt and sugar.
3. Mix with your hands (make sure they are clean). Gently squeeze the vegetables to try to extract the water from them. Discard the water.
4. Boil the water.
5. Weigh out the sugar into another mixing bowl or jug, add in the boiled water after it has cooled a bit, follow with the vinegar. Mix until the sugar dissolves.
6. Put the vegetables into your jar, then add the pickling solution. Prod with a clean implement to remove air bubbles.
7. Store in the fridge.


