---
title: date and walnut loaf
description: "lumpy cake"
date: 2024-04-04
toc: true
t* ags:
  - deserts
---

## ingredients

* 125g unsalted butter, cubed and softened, plus extra for the tin
* 200g soft dates, pitted and roughly chopped
* ½ tsp bicarbonate of soda
* 150g light muscovado sugar
* 2 eggs
* 1 tsp baking powder
* 225g self-raising flour
* 100g walnuts, roughly chopped
* 1 tbsp icing sugar

## instructions

1. Heat the oven to 180C/160C fan/gas 4. Butter a 2lb loaf tin (ours was 22 x 10cm and 6cm deep) and line with baking parchment.
1. Put the butter, dates and bicarbonate of soda (which helps break down the dates) in a large bowl and add 150ml boiling water.
1. Give it a stir so the butter melts, and leave to stand for around 20 mins until the dates have softened.
1. Whisk in the sugar until smooth, then gradually whisk in the eggs until combined.
1. Beat in the baking powder, flour and a pinch of sea salt until there are no lumps of flour left and it’s smooth.
1. Fold in three-quarters of the walnuts, then pour into the prepared tin.
1. Sprinkle the remaining walnuts over the top.
1. Bake for 20 mins, then turn the oven down to 170C/150C fan/gas 3 and cook for another 45-50 mins until golden brown and a skewer inserted into the centre comes out clean.
1. Leave the cake to cool in the tin for 15 mins, then transfer to a cooling rack to cool completely.
1. Lightly dust with the icing sugar and serve in slices.

## storage

* Freeze whole or cut into slices, then place in an airtight container or divide between reusable freezer bags.
* Can be frozen for up to three months.
* Defrost thoroughly overnight.
