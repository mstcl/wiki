---
title: braised eggplant
description: essential oils and co
toc: true
tags:
  - asian-food
---

## ingredients

* 2 small Chinese eggplants (11 ounces, or 300 g, total), stems removed and cut
  into bite-size pieces
* 1 tablespoon light soy sauce (or soy sauce)
* 1/2 teaspoon dark soy sauce
* 2 teaspoons sugar
* 1 teaspoon plus 1 tablespoon cornstarch, divided
* 3 1/2 tablespoons peanut oil (or vegetable oil), divided
* 1 teaspoon minced ginger
* 3 cloves garlic, chopped
* Sliced scallions, for garnishing

## instructions

### prepare the eggplant

1. Place the eggplant pieces in a large bowl and add water to cover.
1. Add the salt and mix well.
1. Set a pot lid on top of the eggplant to keep it under water.
1. Let soak for 15 minutes.
1. Strain and pat dry.

### make the sauce

1. In a small bowl, combine the light and dark soy sauces, 1 tablespoon of
   water, the sugar, and 1 teaspoon of the cornstarch for the sauce.
1. Stir until the cornstarch is dissolved.
1. Sprinkle the eggplant with the remaining 1 tablespoon cornstarch
1. Mix by hand until the eggplant is evenly coated with a thin layer.

### putting it all together

1. In a large nonstick skillet, heat 3 tablespoons of the oil over medium-high
   heat until hot.
1. Spread the eggplant pieces in the skillet without overlapping.
1. Fry the eggplant for 8 to 10 minutes, flipping halfway through, until all
   the surfaces are golden brown and the insides turn soft.
1. If the skillet gets too hot and starts to smoke, reduce the heat to medium.
1. Transfer to a large plate.
1. Add the remaining 1⁄2 tablespoon oil, the ginger, and the garlic to the same
   skillet over medium heat, stirring a few times until fragrant.
1. Add the eggplant back into the skillet.
1. Stir the sauce again to make sure the cornstarch is dissolved, then pour it
   over the eggplant.
* The sauce reduces quickly, so immediately stir a few times until the
  eggplant is evenly coated and the sauce thickens.
1. Transfer the contents of the pan to a large serving plate, top with the
   scallions, and serve hot.

## notes

* You can use regular eggplant and still get a crispy texture; however, Chinese
  eggplant is the best option.
* Normal eggplants take longer to cook. Be patient!
