---
title: sweet bean paste noodles
description: "a jar of sauce rules all"
date: 2024-04-04
toc: true
tags:
  - asian-food
---

This was inspired by Dan Dan noodles but it seems to have diverged quite a bit.
The result is a spicy, sweet, saucy noodle. This is not really a whole dish,
but just a combination of sauces you can add to your noodles. Cucumber adds
freshness.

----

## ingredients

* Sweet soybean paste, 2 tablespoons per serving
* Dark soy sauce
* Sichuan peppercorns, 1 heaped teaspoon per serving
* Chilli oil
* Sesame oil
* Cucumber

## instructions

1. Make noodles
2. Crush sichuan peppercorns, with mortar and pestle, or whatever you have lying around.
3. Chop cucumber into thin strips
4. Put noodles in serving bowl
5. Add all of the ingredients and mix.

## notes

* To make the noodles more fragrant, you can use the hot oil method as in the
  [[biang_biang_noodles|biang biang noodles]] recipe, and then add these sauces.
* Serve with some stir-fried tofu on the side for a main meal.

