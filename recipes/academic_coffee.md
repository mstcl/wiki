---
title: academic coffee
description: gas station coffee
toc: true
tags:
  - coffee
---

This is a weakly-brewed variant of [filter coffee](filter-coffee). This recipe aims to
replicate the flavour of coffee produced by the automatic filter machines found
in american gas stations or in academic offices. It is very drinkable and
easy-going. However, your polemics should be anything but!

______________________________________________________________________

## notes

This recipe works well with darkly-roasted beans with dark, roasty,
nutty/chocolatey flavours, such as darkly roasted south american coffee
(Brazil, Nicaragua, etc.) or other coffee marketed as "Italian" or "French"
roast or style.

This coffee is best prepared as a large batch and then drunk over many hours
whilst working on your latest manuscript.

In the absence of owning a large carafe, using a cafetiere without the lid
works well.

## coffee matrix

| Brewer         | Beans | Grind    |
| -------------- | ----- | -------- |
|   [[filter]]   | 25g   | 40 niche |

## method

1. Boil a large volume of water
1. Measure and grind your coffee
1. Once the water has boiled, rinse the filter with the hot water by placing it
   in the filter holder and pouring some water through.
1. Add your coffee grinds to the filter and place it on top of your vessel.
1. Pour the hot water over the grounds, aiming to thoroughly wet all of them.
1. Let the water drip through
1. Notice that there should be a groove in the grounds, now pour water around
   the edges of the filter to wash the grounds back to the middle.
1. From here on, keep adding water when there is space, in a gentle, circular
   motion, until you have reached your desired volume of beverage.

<pre style="background-color: transparent; border-width: 0px; line-height: .9rem"><code style="background-color: transparent">
         {
      {   }
       }_{ __{
    .-{   }   }-.
   (   }     {   )
   |`-.._____..-'|
   |             ;--.
   |            (__  \
   |             | )  )
   |             |/  /
   |             /  /
   |            (  /
   \             y'
    `-.._____..-'
</code></pre>
