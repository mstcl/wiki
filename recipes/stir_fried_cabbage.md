---
title: stir fried cabbage
description: "who knew cabbage was so good"
date: 2024-04-04
toc: true
tags:
  - asian-food
---

I discovered almost accidentally that if you aggressively cook cabbage, and let
it singe a bit, you get a very toasty, satisfying, almost meaty flavour. Then
it just smelled like it was begging for some hoisin sauce, so naturally I
obliged. If you keep it a little watery and don't let it dry out, you can serve
it with noodles and get a very tasty, slightly sweet dish. The creation of this
dish was purely vibes-based and as such there are no measurements.

----

## ingredient

* Half of a sweetheart cabbage (the harder white leaves inside work better than the outer green leaves), sliced as below
* Light soy sauce
* Brown sugar
* Hoisin sauce
* Ginger, chopped
* Chinese 5-spice
* (optional) Tofu

## instructions

### cabbage

1. You can either peel the leaves off the cabbage and then slice it, or you can slice the cabbage from top down.
1. The aim is to have lots of thin strips of cabbage, around 1 cm in width.
1. Each strip should have part of the leaf midrib in it. (ie. a transverse section)

### cooking

1. Heat up your wok/pan to a high heat.
1. Add the sliced cabbage to the pan.
1. Allow the cabbage to almost burn in small spots, mix it a little to try and singe most of the pieces.
    * You should smell the toasty aromatic.
1. Reduce the heat to medium and add a bit of vegetable oil.
1. Add a sprinkle of brown sugar and a pinch of salt.
1. Add chopped ginger.
1. Add a small sprinkle of 5-spice.
1. Stir for a minute or so,
1. Now add a lid to the pan, and allow the cabbage to soften. You may need to reduce the heat again. It should not be sizzling aggressively at this stage.
1. After about 5 minutes, the cabbage should have softened enough, we still want a bit of bite from the midrib of the leaf.
1. Reduce heat to medium-low.
1. Add a splash of light soy sauce, and the Hoisin sauce. I prefer to add 1 tablespoon, to let the cabbage taste come through.
1. Stir to incorporate the sauces.
1. At this point you can add tofu if you wish.
1. Serve with noodles.
