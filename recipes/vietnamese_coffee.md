---
title: vietnamese coffee
description: cà phê đen nóng
date: 2024-04-04
toc: true
tags:
  - coffee
---

## notes

We like [Trung Nguyen Legend](https://trungnguyenlegend.com/) beans, but there are probably many other brands that have the proper Viet style. The classic Vietnamese coffee is often a blend of _C. arabica_ and _C. robusta_, and is roasted thoroughly; the beans are dark and shiny.

## coffee matrix

| Brewer       | Beans | Grind         |
| ------------ | ----- | ------------- |
|   [[phin]]   | 22g   | Medium-Coarse |

## method

For hot, black coffee (đen nóng):

1. Boil enough water to fill your phin.
1. Measure and grind your coffee.
1. Place phin upon coffee receptacle, remove lid and filter.
1. Add ground coffee to phin, replace filter on top of coffee, screw down if required.
1. Add freshly boiled water, slowly, on top of the filter.
1. Replace phin lid.
1. Wait for the water to drip through to the cup.
1. Take the lid off, and place it upside down on the table.
1. Remove the phin and place on the upturned lid.
1. Enjoy the coffee.
