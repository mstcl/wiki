---
title: list open ports
tags:
  - cli
  - networking
---

```bash
$ sudo ss -tulpn
```

(mostly comes installed)

```bash
$ sudo netstat -tulpn
```

(needs net-tools)
