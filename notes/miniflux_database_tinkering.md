---
title: miniflux database tinkering
date: 2024-05-01
tags:
  - postgres
  - selfhost
---

Useful snippets for administrating a Miniflux instance.

## updating the oidc id

```sql
UPDATE users
SET openid_connect_id = '<new OIDC id>' WHERE id=<user id>;
```
