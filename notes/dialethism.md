---
title: dialethism
tags:
  - theory
---

A philosophical methodology that searches for truth by examining contradictions
and relationships. This can be through rhetorical discourse between 2 or more
parties. The method consists of posing an initial thesis, the opening argument.
Then, an antithesis is provided, the counter-argument. This results in the
creation of a synthesis, an improvement on both the thesis and antithesis,
which is closer to the truth.
