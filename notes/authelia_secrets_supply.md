---
title: supply authelia secrets
tags:
  - selfhost
---

Writing this here instead of relying on the official documentation because it
is frankly a mess.

## set inside the configuration

A naive way to supply secrets to Authelia is in the config file, but this is
not secure. Secrets should be passed using files.

## using secrets files

Set the configuration keys in the config file and set the path of the files
holding secrets using the correct environment variables.

It may seem like you can write the secrets into the environment variable
directly by removing the `_FILE` from the environment variable's name, but
**this does not work**, for some reason.

https://www.authelia.com/configuration/methods/secrets/#environment-variables

## as docker secrets

Docker also has support for it's own secrets system, which mounts a secrets
file onto the container containing the secret. Authelia supports this.

https://www.authelia.com/integration/deployment/docker/#using-secrets
