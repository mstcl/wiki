---
title: save notifications with fzf
description: without pesky popups
date: 2024-05-29
tags:
  - fzf
---

## requirements

- [fzf](https://github.com/junegunn/fzf)
- a terminal (example uses alacritty)
- xorg (might work on wayland)
- [dunst](https://github.com/dunst-project/dunst) (a dbus notification daemon)
- jq

## dunst sample configuration

Dunst can be configured to basically be on do-not-disturb mode all the time,
except when the notification is transient (sent with `notify-send -e`).

This requires some notification filters:

```ini
# ~/.config/dunst/dunstrc

# don't display notifications unless
# they're transient
[skip-display]
    summary = "*"
    skip_display = yes
    match_transient = no

# don't save transient notifications
# to history
[transient_discard]
    history_ignore = yes
    match_transient = yes

# example override: prevent flameshot
# notifications from being hidden and saved
[screenshot]
    summary = "*Flameshot*"
    skip_display = no
    history_ignore = yes
```

## fzf script

```sh
#!/bin/bash

source ~/.local/bin/fzf_defaults

dunstctl history | jq -r \
     '.data[0][] | "\(.id.data)\t\u001b[1;33m\(.summary.data)\u001b[0m\t\(.appname.data)"' |
	  fzf --ansi -d '\t' --with-nth '2,3' \
	    --preview "dunstctl history | jq -c '.data[0][] | select(.id.data == {1})' | jq -r '.body.data'" \
		--header=$'\e[1;34m<ctrl-x>\e[0m exit & clear history\n\n' \
		--border-label='Saved notifications' \
		--bind 'ctrl-x:execute(dunstctl history-clear)+abort'
```

This will launch fzf showing notification history, where each entry contains
the summary and the app name. The preview window shows the notification body,
which, to my best ability, had to be a separate query to avoid newlines ruining
the fzf input.

Additionally, this also binds `ctrl-x` to exit the window and clear all
history.
