---
title: manage todos with fzf
description: using the vTODO standard
date: 2024-05-29
tags:
  - fzf
---

## requirements

- [fzf](https://github.com/junegunn/fzf)
- a terminal (example uses alacritty)
- xorg (might work on wayland)
- [vdirsyncer](https://github.com/pimutils/vdirsyncer) - synchronize caldav
  - set this up to sync files
- [todoman](https://github.com/pimutils/todoman) - assist with vTODOs

## printing todos and description in fzf

```sh
#/usr/bin/env bash

todo --porcelain list |
	jq -r '.[] | "\(.id|tostring)\t\(.summary)"' |
	fzf --with-nth 2 -d "\t" \
		--header=$'\e[1;34m<ctrl-e>\e[0m edit \e[1;34m<ctrl-d>\e[0m mark done' \
		--preview-window "hidden" \
		--preview "todo -h show {1}" \
		--bind 'ctrl-n:preview-up' \
		--bind 'ctrl-p:preview-down' \
		--bind='ctrl-e:become(todo edit {1})' \
		--bind='ctrl-x:become(todo done {1})'
```

This binds `ctrl-x` to mark a todo as done, and `ctrl-e` to open todoman's TUI
to edit the task. Fetching the preview is a bit slow, so it is hidden by
default. Actually, the whole thing is a bit slow, but it is still acceptable.

## what it cannot do yet

todoman doesn't show
[subtasks](https://github.com/pimutils/todoman/issues/396), which is a shame.
