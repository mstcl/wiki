---
title: a memo on argument parsing
description: commands vs switches
date: 2024-04-21
tags:
  - cli
---

## p7zip

[p7zip](https://sourceforge.net/projects/p7zip/) is a port of 7z (a rare piece
of free software for windows) to UNIX systems.

like 7z, p7zip uses "function letters", this means stuff like extract, add, delete, list
is done as a command, without a `-` at the front of the letter.

Extracting can be done with:

```bash
$ 7z x <input> -o <output>
```

The `x` makes sense---it's one of many actions in the action tree. This style
is popular, e.g. `tailscale status` or `docker network ls`.

## tar

tar uses switches and allows both single `-` for "UNIX-style" and `--`
for "GNU-style"

Extracting can be done with:

```bash
$ tar -xf <input> <output>

# OR

$ tar --extract <input> <output>
```
But it also works if you omit the switches. It's kinda confusing but that's how
it is. At least it's consistent with forcing everything to be switches.

Even though it's less intuitive than the "action-first" approach, it's arguably
easier to implement and more consistent; we don't have to guess if a thing is
an action or an option. Although surely that's obvious? Not quite, we can use
`docker container ls` for listing containers but then it's `playerctl -l` for
listing available mpris2 players. Both have command-like invocations and
switches. Maybe it's easier to just do one or the other.
