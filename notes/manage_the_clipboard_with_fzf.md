---
title: manage the clipboard with fzf
description: gather your clippings in one place
date: 2024-05-19
tags:
  - fzf
---

## requirements

- [fzf](https://github.com/junegunn/fzf)
- a terminal (example uses alacritty)
- xorg (might work on wayland)
- [clipcatd](https://github.com/xrelkd/clipcat) (a clipboard manager)
  - set this up according to their instructions first, but don't worry about
    `clipcat-menu`, we will make our own.

## script

```bash
# fzf_clipcat.sh
#!/bin/bash

cid=$(clipcatctl list | sort | fzf -d ': ' --with-nth '2' --preview 'echo $(clipcatctl get {1})' \
    --header=$'\e[1;34m<ctrl-x>\e[0m exit & clear \e[1;34m<ctrl-r>\e[0m reload\n\e[1;34m<enter>\e[0m copy\e[1;34m<left>\e[0m remove\n\n' \
    --border-label='Cached clipboard' \
    --bind 'enter:become(echo {1})' \
    --bind 'ctrl-x:execute(clipcatctl clear)+abort' \
    --bind 'ctrl-r:reload(clipcatctl list)' \
    --bind 'left:execute(clipcatctl remove {1})+reload(clipcatctl list)' \
    --bind 'ctrl-n:preview-up' \
    --bind 'ctrl-p:preview-down')


if [[ $cid == "" ]]; then
	exit
fi

clipcatctl promote $cid
```

This will allow executing `ctrl-r` to reload, `ctrl-x` uto clear and quit,
`<left>` to remove an entry, and `enter` to promote the entry to our current
clipboard and quit. See
[[launch_applications_with_fzf#launch-with-an-alacritty-window|here]] for how
to integrate this with alacritty to replace dmenu.
