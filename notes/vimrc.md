---
title: server .vimrc
date: 2024-09-08
tags:
  - cli
---

```vim
set background=light
colorscheme desert
set nocompatible
set autoread
set undofile
set undolevels=500
set title
set encoding=utf-8
set showcmd
set list
set listchars=extends:›,tab:▸\ ,trail:×
set mouse=a
set number
set relativenumber
set cursorline
set textwidth=80
set diffopt+=iwhite
set whichwrap=bs<>[]
set laststatus=2
autocmd BufReadPost *
	\ if line("'\"") > 0 && line("'\"") <= line("$") |
	\ 	exe "normal g`\"" |
	\ endif
set showbreak=>\ \ \
set ofu=syntaxcomplete#Complete
set ttyfast
set tabpagemax=100
set wildignorecase
set hlsearch
set incsearch
set ignorecase
set incsearch
set showmatch
set mat=2
set linebreak
set wrap
set colorcolumn=88
set smartcase
set gdefault
set copyindent
set smarttab
set list
set autoindent
set smartindent
set shiftwidth=4
set shiftround
set tabstop=4
set wildmenu
set splitbelow
set splitright
set nolazyredraw
set stal=2
set backspace=indent,eol,start

syntax enable
filetype plugin indent on
```
