---
title: security with rathole
description: using nginx to forward proxy protocol
date: 2024-04-21
tags:
  - selfhost
  - networking
  - security
---

## what is rathole

"[rathole](https://github.com/rapiz1/rathole)" is a performant and lightweight
reverse proxy for NAT traversal. Out of the box, it does not forward packets
that allow the server (rathole's clients) to identify the clients (those who
are accessing the page) address. This is addressed by the [proxy
protocol](https://www.haproxy.org/download/1.8/doc/proxy-protocol.txt).

## implementing the proxy protocol with nginx

A workaround was
[proposed](https://github.com/rapiz1/rathole/issues/221#issuecomment-1432826561)
using nginx's
[stream](https://docs.nginx.com/nginx/admin-guide/load-balancer/using-proxy-protocol/#proxy-protocol-for-a-tcp-connection-to-an-upstream)
module. It is possible to send along data using the proxy protocol like this:

```nginx
# /etc/nginx.conf

events {}

stream {
    server {
        listen          80;
        proxy_pass      rathole:80;
        proxy_protocol  on;
        error_log off;
    }
    server {
        listen          443;
        proxy_pass      rathole:443;
        proxy_protocol  on;
        error_log off;
    }
}
```
