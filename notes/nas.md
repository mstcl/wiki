---
title: nas drives
tags:
  - hardware
  - selfhost
---

## order

from left to right, serial ending in:

- WD-\*\*K1Y
- WD-\*\*4N4
- WD-\*\*F6E
- WD-\*\*P6V

## spare drive

- WN\*\*QRZ
