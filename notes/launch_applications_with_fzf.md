---
title: launch applications with fzf
description: replace dmenu
date: 2024-05-02
dateUpdated: 2024-05-19
tags:
  - fzf
---

## requirements

- [fzf](https://github.com/junegunn/fzf)
- a terminal (example uses alacritty)
- xorg (might work on wayland)
- something to fetch desktop files (i3, j4-dmenu-desktop)

## floating windows for tiling wms

Many terminals has the ability to set a window's class. The above script
launches an alacritty window with the `launcher` class

In i3wm, make this floating with

```
# ~/.config/i3/config

# floating
for_window [class="^(launcher)$"] floating enable

# window dimension width=800 height=678
for_window [class="^(launcher)$"] resize set 800 678

# set at the middle of the screen
for_window [class="^(launcher)$"] move position center

# disable window borders
for_window [class="launcher"] border pixel 0
```

## display desktop entries

Those pesky `.desktop` files in `/usr/share/applications` and
`~/.local/share/applications` are gathered by the usual taskbars and dmenus.
There is i3-menu-desktop that comes with i3, and j4-menu-desktop that runs
standalone, both allow the `--dmenu=` switch to be set to a fzf command.

Here's a sample script to get it working.

```bash
# fzf_launcher.sh
#!/bin/bash

i3-dmenu-desktop --dmenu="fzf --border-label='Launcher' --bind 'enter:accept'"
```

Tweak `FZF_DEFAULT_OPTS` to configure fzf for the script only (the variable set
elsewhere won't be overridden).

## launch with an alacritty window

We can launch the above script inside a terminal such as alacritty, invoked by
a keybinding. Alacritty can take a bit to launch, but it has an API to launch a
new window from an existing and running Alacritty instance. We can leverage
[this](https://github.com/alacritty/alacritty/issues/782#issuecomment-1315519751)
to get faster startup time. There might be some visual artefacts due to the
lack of animations, but whether this is a problem a matter of taste. The script
can be placed on our `PATH` for convenience, and can be invoked with:

```bash
$ alacritty msg create-window --class=launcher --command fzf_launcher ||\
  alacritty --class=launcher --command fzf_launcher
```

To make this look nice, we can also set fzf options like `--margin=0,0` and
`--padding=0,0`.
