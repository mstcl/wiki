---
title: division of labour
tags:
  - theory
---

The division of labour is a necessary condition for commodity production.

Commodity production is not a necessary condition for the division of labour.
