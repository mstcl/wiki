---
title: handy tools
description: for the tired sysadmins
---

[cidr.xyz](https://cidr.xyz/) - explain CIDR range

[crontab generator](https://crontab.cronhub.io/) - explain crontab expressions

[pgsql tuner](https://pgtune.leopard.in.ua/) - PostgreSQL tuning based on requirements

[jinja parser](https://j2live.ttl255.com/) - jinja parser

[certificate search](https://crt.sh/) - search all known certificates of a domain

[decomposerize](https://www.decomposerize.com) - convert docker compose to docker run

[composerize](https://www.composerize.com/) - convert docker run to docker compose

[explain shell](https://www.explainshell.com) - explain each part of common shell commands

[abuse ip db](https://www.abuseipdb.com) - database of abused ips

[dive](https://github.com/wagoodman/dive) - explore each layer in a docker image
