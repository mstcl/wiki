---
title: duhem
tags:
  - theory
---

Also known as **Duhem-Quine thesis** or confirmation holism. Designed to refute
Popper's naive [[falsificationism]].

There is a problem with disconfirming a falsifiable theory, i.e., a theory
cannot be tested in isolation.

It is never just H that predicts e, but rather H + A, or auxiliary statements.

**Example**: initial conditions, measuring devices and detectors.

## argument

```
H and A gives e
not e
therefore not (H and A), that is to say
therefore not H or not A
```

## cases

- Anthrax caused by bacteria but technical limits with microscopes.
- Null results from detecting motion through the ether because no ether.
- Errors in Uranus orbits and discovery of Neptune but not throwing out
  Newtonian gravitation.
- Errors in Mercury orbits and general relativity but not Vulcan.
