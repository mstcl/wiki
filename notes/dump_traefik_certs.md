---
title: dump traefik acme.json
date: 2024-04-21
tags:
  - selfhost
  - traefik
  - networking
---

Assuming traefik is running as user 3000 and the current directory contains `acme.json`:

```sh
$ docker run --rm --user 3000:3000 -v $(pwd):/dump \
    ldez/traefik-certs-dumper:v2.8.3 file \
        --source /dump/acme.json \
        --clean false \
        --dest /dump/dump \
        --version v2 \
        --crt-ext=.pem \
        --key-ext=.pem \
        --domain-subdir=true
```
