---
title: domestic zfs
description: zed eff ess
tags:
  - selfhost
  - filesystem
---

## note to self

- use this [terraform provider](https://github.com/MathiasPius/terraform-provider-zfs)
- allow a user to run zpool and zfs without creds
- create pool with mirrored vdevs topology
- pool properties configured for both extremes (both database and large media)
- create dataset
- extend pool by adding further mirrored vdevs (need even drives)
- set up NFS share

## some useful commands

```bash
$ zfs get <property> <tank>/<dataset> # list a property
$ zfs get all <pool>/<dataset>        # list all properties
$ zpool list <pool>                   # list pool
$ zfs list                            # list pools and datasets
```
