---
title: miniflux retries
date: 2024-06-30
tags:
  - selfhost
---

Feeds often timed-out with the errors like this[^1]:

```
dial tcp: lookup <url> on 127.0.0.11:53: server misbehaving
```

[^1]: `dig @127.0.0.11:53 url` inside the container shows no problem whatsoever.

This issue is probably due to a low connection timeout limit. After a few
errors Mini flux will cease to retry automatically. However, retrying manually
is not an option as this occurs several times a day. From this
[issue](https://github.com/miniflux/v2/issues/306), there is a flag called
`-reset-feed-errors` that will remove error counts on all feeds so that
Miniflux will attempt to fetch the feed again. Run this as a cron job makes a
pretty acceptable workaround for this annoyance.
