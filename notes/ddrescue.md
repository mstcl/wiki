---
title: ddrescue
description: you will need it one day
tags:
  - filesystem
---

## whole disk rescue

Scenario:

- The bad disk is on `/dev/sdc`.
- We want to recover an image to `/dev/sdb`, which needs to have space larger
  than the size of `/dev/sdc`. Mounted at `/mnt/b`

using ddrescue:

```bash
$ sudo ddrescue -r 1 -d /dev/sdc /mnt/b/c_recovered.img recover.log
```

- retry once if it runs into bad sectors
- log file, so we can resume after `Ctrl-C`
- direct access

## mounting recovery image

Download parted, use it to list the partition end bytes, then mount with:

```bash
$ mkdir /mnt/b/browse_recovered
$ sudo mount -t ext4 -o loop,offset=529387 c_recovered.img /mnt/b/browse_recovered
```
