---
title: remove broken symlinks
date: 2024-05-19
tags:
  - cli
---

## find

```bash
$ find . -xtype l -delete -print -delete
```

## fd
```bash
$ fd . -L --type l -x rm -v
```
