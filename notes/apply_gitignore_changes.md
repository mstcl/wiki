---
title: apply gitignore changes
date: 2023-08-04
tags:
  - cli
---

```bash
$ git rm -r --cached <newly ignored stuff>
$ git commit -m <message>
```

https://stackoverflow.com/questions/38450276/force-git-to-update-gitignore
