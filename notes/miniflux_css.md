---
title: miniflux css
date: 2024-09-08
tags:
  - selfhost
---

```css
:root {
  --foreground: #222;
  --sub: #666;
  --background: #f8f8f8;
  --subtle:#eee;
  --hover:#dfdfdf;
}

html {
  line-height: 1.75;
}

body {
  font-family:sans-serif;
  background-color:var(--background);
  -webkit-text-size-adjust:100%;
  font-size:1rem;
  font-weight:400;
  padding:1em;
  margin:auto;
  max-width:42rem
}

* > a {
  color:var(--foreground) !important;
}

* > a:visited {
  color:var(--foreground) !important;
}

* > a:active {
  color:var(--foreground) !important;
  background:var(--hover) !important;
}

.item {
  background-color: var(--subtle);
  outline: 0;
  border-radius: .25rem;
  border-color: var(--hover);
  border-style: solid;
  padding: .5rem;
}

.item-header h2 {
  font-size: 1.05rem;
}

.item-title a {
  color: #111;
}

.item.current-item {
  outline: 0;
  border-color: var(--sub);
  padding: .5rem;
  border-width: 1px;
}

.item-title a img,
.item-meta-icons-external-url a svg,
.item-meta-icons svg,
.entry-website img,
.entry-actions .icon,
.pagination-next::after,
.pagination-prev::before,
.page-button svg,
.logo a,
.page-link svg,
.page-header svg
{
  display: none;
}

.page-header h1,
.entry-header h1
{
  border-width: 0px;
  margin-bottom: 1rem;
  margin-top: 2.5rem;
  color: var(--foreground);
  font-size: 2rem !important;
}

.entry-header {
  border-bottom: 1px dotted var(--hover) !important;
}

.header li {
  list-style: none;
}

@media screen and (min-width:768px) {
  body {
    margin: auto 5rem
  }
}

.page-header nav ul li a,
.item-meta-icons li > :is(a, button),
.item-meta-icons a span,
.item-meta-info a,
.page-header li button,
.entry-header li button,
.entry-actions a span,
.page-footer li button,
.header li a,
.entry-tags,
.entry-tags strong,
.entry-tags a,
.entry-meta,
.entry-date,
.entry-website a,
.pagination a,
.page-link
{
  color: var(--sub) !important;
  text-decoration: none !important;
  font-style: normal !important;
}

.category {
  color: var(--sub);
  background-color: var(--hover);
  border-color: var(--hover)
}

.page-header nav ul li a,
.item-meta-info,
.page-button,
.item-meta-icons li > :is(a, button),
.item-meta-icons a span,
.item-meta-info a,
.entry-actions button,
.entry-actions a,
.pagination,
.entry-tags,
.entry-tags strong,
.entry-tags a,
.entry-meta,
.entry-date,
.entry-website a,
.page-link
{
  font-size: .9rem
}

.entry-content {
  font-size: 1rem;
  color: var(--foreground);
  line-height: 1.5;
}

.entry-content a {
  text-decoration:underline;
  text-decoration-thickness:2px;
  text-decoration-color:var(--hover);
  color:var(--foreground)
}
.entry-content a:visited {
  color:var(--foreground)
}
.entry-content a:hover {
  text-decoration:var(--sub)
}
.entry-content a:active {
  color:var(--foreground);
  background:var(--hover)
}

fieldset, input, .button, textarea {
  border-radius: .25rem;
}

fieldset {
  border: 1px solid var(--subtle);
}
```
