---
title: cache traefik with souin
tags:
  - selfhost
  - traefik
  - networking
---

[Souin](https://github.com/darkweak/souin) is an HTTP cache system that works
with traefik. It can be used as a traefik
[plugin](https://plugins.traefik.io/plugins/6294728cffc0cd18356a97c2/souin) .

Memory leaks during testing was experienced. Traefik racked up err 9 GB of
memory usage.
