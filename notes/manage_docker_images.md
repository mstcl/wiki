---
title: manage docker images
date: 2024-06-07
tags:
  - selfhost
---

- don't want to run `$ docker image prune -a` because there are ad hoc images
  that are used occasionally.

## sort by registry name

```sh
$ docker image ls | sort
```

## sort by tags & delete old versions by registry name

```sh
$ docker image ls --format="{{.Tag}}\t{{.ID}}\t{{.Repository}}" |
    sort |
    grep <name> |
    cut -f 2 |
    tr '\n' ' ' |
    sed 's/ $/\n/'
```

All references except the last one can be deleted.
