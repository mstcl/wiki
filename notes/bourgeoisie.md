---
title: bourgeoisie
tags:
  - theory
---

The ruling class. Owners of the [[means_of_production|means of production]], or
the buyers of labour.
