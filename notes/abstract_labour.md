---
title: abstract labour
tags:
  - theory
---

The concept of human labour without any regard to any specific production or its output value.
