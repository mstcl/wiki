---
title: wireguard
date: 2024-04-05
tags:
  - networking
---

## preliminary guide

Follow this guide to setup Wireguard server and client

https://wiki.debian.org/WireGuard

## UFW setup

* On the WG server: unblock Wireguard port (default 51820/UDP)
* On the WG server and clients with UFW: allow traffik on the `wg0` interface.

https://unix.stackexchange.com/questions/530954/ufw-blocks-wireguard
```bash
$ sudo ufw allow in on wg0 to any
```

## NAT-NAT communication

https://staaldraad.github.io/2017/04/17/nat-to-nat-with-wireguard/
