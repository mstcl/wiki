---
title: finance capital
tags:
  - theory
---

The use of financial tools such as loans and interest, securities,
share-holding and other investments to exert control and dominance by [[imperialism|imperialist]] powers. Merging of bank capital and industrial
capital, that is, the deepening of the relationship between them.
