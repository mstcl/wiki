---
title: self-host services with oidc integrations
date: 2024-05-01
tags:
  - selfhost
---

Not sure if there's a list like this somewhere already, but here's one

- jellyfin[^1]
- miniflux
- sftpgo
- forgejo
- immich
- opengist
- netbird
- portainer

Issue trackers for others that aren't supported

- [vaultwarden](https://github.com/dani-garcia/vaultwarden/pull/3899)
- [actual](https://github.com/actualbudget/actual/pull/1164)

[^1]: as a plugin, so it doesn't work with clients:
    https://github.com/9p4/jellyfin-plugin-sso
