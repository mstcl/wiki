---
title: duplicate key violation
tags:
  - postgres
---

```sql
DELETE FROM <affected-table> a USING (
    SELECT MIN(ctid) as ctid, <affected-key>
    FROM <affected-table>
    GROUP BY <affected-key> HAVING COUNT(*) > 1
) b
WHERE a.<affected-key> = b.<affected-key>
AND a.ctid <> b.ctid;
```

https://stackoverflow.com/a/12963112
