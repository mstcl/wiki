---
title: commodity
tags:
  - theory
---

From Capital, Vol. 1 Ch. 1:

> "The commodity is, first of all, an external object, a thing which through
> its qualities satisfies human needs of whatever kind."
