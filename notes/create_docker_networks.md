---
title: create docker networks
tags:
  - selfhost
  - networking
---

## ipvlan

```bash
docker network create -d ipvlan \
                      --subnet=192.168.1.0/24 \
                      --gateway=192.168.1.1 \
                      -o ipvlan_mode=l2 \
                      -o parent=eth0 my-net
```

## macvlan

```bash
docker network create -d macvlan \
                        --subnet=192.168.1.0/24 \
                        --ip-range=192.168.1.128/28 \
                        --gateway=192.168.1.1 \
                        -o parent=eth0 my-net
```
