---
title: accidental rm
description: not all accidents are happy ones
date: 2024-04-30
tags:
  - filesystem
---

First of all, this will come in handy:

https://gist.github.com/ebautistabar/cca12863e6335d08a019f015f53fac4a

It details how to use ext4magic to get back deleted files.

In a nutshell, quickly stop all services accessing the device, make a copy of
the filesystem journal, optionally, list the files, then restore them.

Also, ext4magic is kinda abandoned, it segfaults to with `free()` pointing to
some invalid pointer. This patch here works:

https://aur.archlinux.org/packages/ext4magic-patch-extent-free

## copy fs journal

Assume target device is `/dev/sda6` and we've accidentally deleted
`/mnt/my/data` when it was mounted. First, unmount `/mnt`. Then copy the
filesystem journal.

```bash
$ sudo debugfs -R "dump <8> /opt/sda6.journal" /dev/sda6
```

## list deleted files

Let's say we deleted this an hour ago.

```bash
$ ext4magic /dev/sda6 \ # our sad device
    -a $(date -d "-1hours" +%s) \ # timestamp to process
    -f my/data \ # ask ext4magic to inspect this path
    -j /opt/sda6.journal \ # use our external journal
    -l # list deleted files
```

## recover them

Let's recover to `/opt/recover`. Make that directory first.

```bash
$ ext4magic /dev/sda6 \ # our sad device
    -a $(date -d "-1hours" +%s) \ # timestamp to process
    -f my/data \ # ask ext4magic to inspect this path
    -j /opt/sda6.journal \ # use our external journal
    -r \ # recover files
    -d /opt/recover # path to put recovered files
```

Good luck, and back up your files.
