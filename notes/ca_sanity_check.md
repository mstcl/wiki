---
title: ca and certs sanity check
description: mtls for dummies
date: 2024-12-15
toc: true
tags:
  - selfhost
  - security
---

## requirements

- `step-ca`
- `step-cli`

## generate the ca

```bash
$ step ca init
$ step certificate install /path/to/root_ca.crt
```

Note that the DNS name should have `localhost` for this demonstration.

This creates a root CA and intermediate CA. The root one is to be distributed
to clients and installed into their trust store, whereas the intermediate one
is used by servers during the handshake[^1]

[^1]: See [here](https://stackoverflow.com/questions/74480918/is-a-root-certificate-ca-required-on-the-client-side-to-establish-an-https-conne/74481011#74481011) for a good explanation

## start the ca server

```bash
$ step-ca --context=foobar
```

## generate some certs

```bash
$ step-cli ca certificate server server.crt server.key \
    --context=foobar --san=localhost --size=4096 --kty=RSA
$ step-cli ca certificate client client.crt client.key \
    --context=foobar --san=localhost --size=4096 --kty=RSA
```

You can now stop the CA server

## start a tls server

```bash
$ openssl s_server -key server.key -cert server.crt \
    -CAfile /path/to/intermediate_ca.crt
```

## start a tls client

```bash
$ openssl s_client -key client.key -cert client.crt -connect localhost:4433
```

You should see a bunch of stuff and this:

```
SSL handshake has read 2283 bytes and written 377 bytes
Verification: OK
```
