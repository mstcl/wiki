---
title: concrete labour
tags:
  - theory
---

The human labour of a specific commodity production with a specific output
value.

**Example:** A carpenter's labour produces furniture with a specific [[use-value]].

Contrast to [[abstract-labour|abstract labour]].
