---
title: change author in git history
date: 2024-02-05
tags:
  - cli
---

## for the latest commit

```bash
$ git commit --amend --no-edit --reset-author
```

## for the whole project

```bash
$ git rebase -r --root --exec "git commit --amend --no-edit --reset-author"
```

## in one go

```bash
$ if [ "$GIT_AUTHOR_EMAIL" = "<old-email>" ];
GIT_AUTHOR_EMAIL="<new-email>";
git rebase -r <some commit before all of your bad commits> \
--exec 'git commit --amend --no-edit --reset-author'
```
