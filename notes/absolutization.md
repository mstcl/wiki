---
title: absolutization
tags:
  - theory
---

The conceptualisation of a belief as always logically true, at all times
without exception.
