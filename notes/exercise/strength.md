---
title: strength training
description: "sharp body, sharp mind"
date: 2024-04-04
tags:
  - exercise
---

> There is a saying: Civilize the mind and make savage the body. This is an apt
> saying. In order to civilize the mind one must first make savage the body. If
> the body is made savage, then the civilized mind will follow.

*Mao Tse-tung, A Study of Physical Education, April 1917*
