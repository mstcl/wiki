---
title: baikal and vdirsyncer
date: 2024-05-30
tags:
  - selfhost
---

change webdav authentication type on baikal admin's dashboard
from Digest to Basic to work with vdirsyncer.

## source

https://www.ncartron.org/vdirsyncer-and-baikal.html

https://github.com/pimutils/vdirsyncer/issues/1015
