---
title: how to copy files recursively and preserve file structure
tags:
  - cli
---

```bash
$ find . -name <query> | cpio -pdm <destination>
```
