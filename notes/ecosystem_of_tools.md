---
title: an ecosystem of tools > a second brain
date: 2024-05-28
---

Our thoughts are scattered, nonlinear, complicated. Sometimes we feel the
thought before it actually makes sense. Tasks are often rigid structures. Notes
and ideas are often not. To combine them into one interface seems
counter-intuitive. Ideally we might be able to link between them, hence
one-stop solution like org-mode. However alluring this may be, you're still
more or less tied to Emacs to get most of the benefit and ecosystem.

The fix is to not have a rigid workflow for a fluid process---thinking. Use the
best tool for the job, however you feel like it. Often, these tools come as
standards and specifications, like RSS, Atom, vTODO, Markdown, OPDS, EPUBs, and
your browser (all those pending tabs and bookmarks are also dangling
information). There is absolutely no single tool to integrate it all, without
losing functionality, portability, spontaneity. Put things down where it is
easiest, and move them where they're most accessible later.

To find things from different places, use APIs to pull them in one place, and
use tools like [fzf](https://github.com/junegunn/fzf),
[fd](https://github.com/sharkdp/fd),
[rg](https://github.com/BurntSushi/ripgrep),
[zk](https://github.com/zk-org/zk), your editor, etc. to pull things together.
