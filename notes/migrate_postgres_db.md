---
title: migrate a postgres db
date: 2024-04-10
tags:
  - postgres
---

## dumping

```shell
$ docker exec -t <old_hostname> pg_dump --no-owner --no-acl -U <old_user> <old_db> > dump.sql
```

## creating new db

```sql
CREATE DATABASE <new_db> TEMPLATE template0;
ALTER DATABASE <new_db> OWNER TO <new_user>;
```

## restore

```bash
$ docker exec -i <new_hostname> psql -U <new_user> -d <new_db> < dump.sql
```

