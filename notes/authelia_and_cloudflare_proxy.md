---
title: authelia security considerations with cloudflare
tags:
  - selfhost
---

Firstly, avoid using the Cloudflare orange cloud. If not possible, carry on reading.

If clients are connecting from the WAN with Cloudflare set to proxy the
request, then you are vulnerable to spoofing of the `X-Forwarded-For` header.
This would allow a client to spoof their IP to appear as one of the
`trusted_proxies` in Caddy, thereby bypassing Authelia. A work around is to not
use a Cloudflare proxy with Authelia fronting services.

https://www.authelia.com/integration/proxies/fowarded-headers/#cloudflare

For Caddy, if it is not the first server being connected to the client (for
example, if Cloudflare is set to proxy requests to our addresses), then we must
specify to Caddy which proxies to trust. This is achieved using the
`trusted_proxies` global option in the servers block.

This should be as limited as possible (principle of least trust).

https://caddyserver.com/docs/caddyfile/options#trusted-proxies
