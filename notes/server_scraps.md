---
title: server scraps
tags:
  - hardware
---

case: old deepcool matrexx 30 for about £10

storage:

- old 2 TB spinning drive for about £32
- old 128 GB drive for about £18
- new 1 TB drive for about £30 (shat itself after 4 months, currently in ssd
  jail, see [[ddrescue]])
- old 1 TB spinning drive not sure how much

power supply: old seasonic 80+ gold 600 W for about £35 (a steal)

cooler: old noctua cooler for £37

ram: 2 old sticks of 8 GB DDR3 for £24
