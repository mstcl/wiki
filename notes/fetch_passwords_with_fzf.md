---
title: fetch passwords with fzf
description: fast & easy bitwarden client
date: 2024-05-10
dateUpdated: 2024-05-19
tags:
  - fzf
---

## requirements

- [fzf](https://github.com/junegunn/fzf)
- a terminal (example uses alacritty)
- xorg (might work on wayland)
- [rbw](https://github.com/doy/rbw) (a cli for bitwarden/vaultwarden)

## script

```bash
# fzf_pass.sh
#!/bin/bash

# unlock vault if we haven't done so
set -eEuo pipefail
rbw unlocked &>/dev/null || rbw unlock

name=$(rbw ls --fields name,folder,user |
    sort |
    fzf -d '\t' --with-nth 1 \
        --header=$'\e[1;34m<left>\e[0m copy pass \e[1;34m<right>\e[0m copy totp\n\e[1;34m<enter>\e[0m copy pass & quit\n\n' \
        --border-label='Get password' \
        --bind 'left:execute(clipcatctl disable-watcher; rbw get {1} | nohup xclip -loops 0 -r -sel c >/dev/null 2>&1; clipcatctl enable-watcher)' \
        --bind 'right:execute(clipcatctl disable-watcher; rbw code {1} | nohup xclip -loops 0 -r -sel c >/dev/null 2>&1; clipcatctl enable-watcher)' \
        --bind 'enter:become(echo {1})' \
        --preview="echo 'Folder: {2}\nUser: {3}\n'")


if [[ $name == "" ]]; then
	exit
fi

# pause clipboard watcher (if applicable)
clipcatctl disable-watcher > /dev/null

rbw get $name | nohup xclip -loops 0 -r -sel c >/dev/null 2>&1
# (nohup retains clipboard after the script terminates)

# resume clipboard watcher (if applicable)
clipcatctl enable-watcher > /dev/null
```

Pressing `<left>` on an entry will copy its password, and `<right>` will copy
its TOTP, if there is any. Accepting an entry with enter will copy the password
and leave the fzf instance. This way, we can copy many passwords, or passwords
and TOTP, without having to relaunch fzf.

Note that pausing any clipboard watcher, such as
[clipcat](https://github.com/xrelkd/clipcat) is recommended to prevent
passwords from being cached. However, manually copied passwords are still
stored. See [[manage_the_clipboard_with_fzf|manage the clipboard with fzf]] for
how to integrate clipcat with fzf.

See [[launch_applications_with_fzf#launch-with-an-alacritty-window|here]] for
more detailed instructions on how to open this in alacritty window. This allows
us to use a keybinding to bring up a terminal running fzf and grab our
passwords.
