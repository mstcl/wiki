---
title: swap on a VPS
description: download more ram
tags:
  - selfhost
---
This can help you get some more out of a low-end VPS, even when you cannot re-partition the drive.

## creation

Create the swapfile:

```bash
$ touch /var/swap.img
```

Size to your liking:

```bash
$ dd if=/dev/zero of=/var/swap.img bs=1024k count=1000
```

Where `count` is the number of 1024kb size blocks to write to the file. The
above command would give a swapfile of 1 GB. Change `count` to 2000 for 2 GB,
for example.

## initialisation

Initialise swap filesystem:

```bash
$ mkswap /var/swap.img
```

Enable swap space:

```bash
$ swapon /var/swap.img
```

Disable swap space:

```bash
$ swapoff /var/swap.img
```

## make permanent

Add to fstab to enable the swap on boot. Add the following line to
`/etc/fstab`:

```fstab
/var/swap.img none swap sw 0 0
```
