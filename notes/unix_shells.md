---
title: unix shells
tags:
  - cli
description: escargots, etc.
toc: true
---

Not exclusive to Linux or UNIX, a shell is any program with a text interface.
At its most general use, it involves some form of input and output. The form of
these inputs and outputs are facilitated through user interaction or scripting,
or both.

## examples

bash, fish, zsh, sh just to name a few...

## jargon

### interactive vs. non-interactive

An interactive shell is where commands are read from standard input (stdin),
which are fed by user interaction, often through a keyboard. Easy example would
be a GUI terminal.

A non-interactive shell is where commands are not read from stdin, and does not
output to stdout, so outputs are normally written to log files. Examples are
automated processes (like what?).

### login vs. non-login

A login shell is run as a logged in user, and takes all the environments
specific to that login user[^1][^2].

A non-login shell is one run by user after logging on (like typing `zsh` after
starting your terminal, or by an automated process without being logged in.

[^1]: https://stackoverflow.com/questions/18186929/what-are-the-differences-between-a-login-shell-and-interactive-shell

[^2]: https://unix.stackexchange.com/questions/50665/what-is-the-difference-between-interactive-shells-login-shells-non-login-shell

## configuration

We are here to answer the question: **where do I put my environment
variables?**

Often, it is easy to be overwhelmed with the choices, for bash, you have
`/etc/profile`, `~/.bashrc`, `~/.bash_profile`, or even `~/.profile`.

For zsh, it is the same. But from our definitions of shell types above, there
are separate use cases for these files.

### zshenv

Whether this file is placed the home directory or in `/etc`, these are **always
sourced**. Therefore, it is important to **only put the most crucial things**.

- Setting XDG base directories
- Setting `$ZDOTDIR`

### .zprofile

If the shell is a login shell, which, for most display manager, this should be
the case, then zsh will source this file[^3]. Anything user specific goes under
`.zprofile`, such as:

- Application environment variables
- Appending to `$PATH`

### .zshrc

For all interactive shells, zsh will source `.zshrc`. This file is for shell
configuration, such as:

- Your prompt
- Tab completion
- Plugins and plugins configuration

[^3]: https://unix.stackexchange.com/questions/537637/sshing-into-system-with-zsh-as-default-shell-doesnt-run-etc-profile
