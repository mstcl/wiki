---
title: dialectical materialism
tags:
  - theory
---

Applying [[dialectics]] to materialism, and vice versa. It is quite hard to pin
down a set of rules for it, because that would make it dogmatic! But just apply
what is dialectical thinking to a [[materialism|materialist]] worldview, i.e.
believe in [[praxis]].

Sometimes, when people refer to idealism they probably mean someone or
something is being undialectical. But a dialectical movement composes of a
mixture of both, requiring cognition and perception.
