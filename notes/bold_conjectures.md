---
title: bold conjectures
tags:
  - theory
---

A way of proposing theories which is crucial to progress science (claims Popper)

* A formulated hypothesis that is bold, of which we try to disprove
* A bold hypothesis, if true, would be able to predict or explain a lot, or a
  lot more, about the subject being theorised about.

https://en.wikipedia.org/wiki/Bold_hypothesis
