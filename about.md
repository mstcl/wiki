---
title: about
unlisted: true
---

In early 2023, a decade old computer was rescued from being abandoned and
thrown away. While it came with everything needed to boot, to prolong its life,
several bits were replaced by other (slightly less old) bits. Currently, it's
punching and kicking with:

* dual core cpu @ 3.3 ghz
* 16 gb ddr3 memory @ 1333 mhz
* various drives with ghastly power-on hours

Now, this computer runs various things for various people. This place is a
personal wiki for these people so that they don't forget stuff they've learned.
It is generated using [pher](https://github.com/mstcl/pher).

<pre style="background-color: transparent; border-width: 0px; line-height: .9rem"><code style="background-color: transparent">
                ,
              _/((
     _.---. .'   `\
   .'      `     ^ T=
  /     \       .--'
  |      /       )'-.
  ; ,   \<__..-(   '-.)
   \ \-.__)    ``--._)
    '.'-.__.-.
      '-...-'
</code></pre>
