---
title: home
description: "sharing computing resources from a solitary corner of the world"
showHeader: true
unlisted: true
---

## available goodies

* [searxng](https://sxn.lonely-desk.top) - a meta-search engine
* [piped](https://ytb.lonely-desk.top) - a youtube alt-frontend
* [rimgo](https://img.lonely-desk.top) - an imgur alt-frontend
* [redlib](https://red.lonely-desk.top) - a reddit alt-frontend
* [quetre](https://qur.lonely-desk.top) - a quora alt-frontend
* [wikiless](https://wkp.lonely-desk.top) - a wikipedia alt-frontend
* [moodist](https://amb.lonely-desk.top) - an ambient sound generator

<pre style="background-color: transparent; border-width: 0px; line-height: .9rem"><code style="background-color: transparent">
             ,-.
    ,     ,-.   ,-.
   / \   (   )-(   )
   \ |  ,.>-(   )-<
    \|,' (   )-(   )
     Y ___`-'   `-'
     |/__/   `-'
     |
     |
     |
  ___|_____________
</code></pre>
