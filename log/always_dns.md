---
title: it's always dns
date: 2024-06-07
---

After a routine update of docker images that only have the `latest` tags,
Searxng broke after a docker restart. Every single search times out. The
container has internet access. An initial dig shows that requests takes 4
seconds. Using vanilla docker run as opposed to Ansible on a default Searxng
configuration works, so I thought the problem was Searxng. Copying the new
configuration onto the container ran using Ansible. Still didn't work. It must
be DNS then. But dig is fast on host, so what's wrong?

Ansible led me astray, since the `resolv.conf` for normal docker run is
different to the one made by Ansible. I thought the problem must be Ansible
then. Upon closer scrutiny, there were two gateways `192.168.1.1` and
`192.168.19.1`. Huh?

It turns out I changed the router gateway to `192.168.19.1` a few days ago, and
forgot to update the relevant systemd units. After removing `192.168.1.1`,
everything is now okay again.
