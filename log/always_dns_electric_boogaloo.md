---
title: it's always dns - electric boogalo
date: 2024-10-02
---

Today, our certs expire. And today, it failed to renew. At 10:00pm, everything
was down. Suspecting it was the LE API tokens, we rolled it over. Only after
rolling it over and restarting Traefik did we realise the old token's expiry
date was set sometime in the far future. The next suspicion was DNS, but you
can kindly ask Traefik to resolve your domains with another DNS resolve like
this:

```yaml
certificatesResolvers:
  letsencrypt:
    acme:
      storage: /etc/traefik/acme.json
      dnsChallenge:
        provider: cloudflare
        delayBeforeCheck: 0
        resolvers:
          - "1.1.1.1:53"
          - "1.0.0.1:53"
```

So we were like - "d'oh it was not the DNS"... only except it was. A few
restarts after and random guesses, we found that our router had the option
"override DNS for all clients" checked (why was it checked - who knows? Note to
self don't change random things and not reset it back). Unticked that and life
was back to normal.
